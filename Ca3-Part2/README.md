# Ca3-Part2 - Virtualization with Vagrant
___
### Introduction to Vagrant
___

Vagrant is an application that creates and configures virtual development environments.
It can be used as a simpler solution around virtualization software (like VirtualBox, VMware, KVM or Linux Containers (LXC)),  
and configuration management software (like Ansible, Chef, Salt or Puppet).
The most used virtual machine platform on Vagrant is Virtualbox (the one that we use) which is developed by Oracle.
It's cross-platform and can run a large number of operating systems easily, allowing to freely limit, for example, the ammount of RAM, CPU and disk.
In the case of Vagrant, you can, with a few commands, start, provision, and configure Virtualbox so that you can use 
the virtual machine in a intuitive way.
___
### Task3 - Part2 Development

The goal of this task is to use Vagrant to setup a virtual environment to execute the Ca2-Part2(tutorial spring boot aplication using gradle).  
  
* First I created a directory to this task:  
```$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761```  
````$ mkdir Ca3-part2````  
  
* I confirmed that the directory was created and added a copy of the Ca2-part2 directory;  
* Then I copied the **Vagrantfile** that was on https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ to my directory **Ca3-Part1**, confirmed the changes in the Ca3-Part3 that
were uncommited and pushed them to the remote repository:  
````$ git status````  
````$ git add Ca3-part2````  
````$ git commit -m "Create new directory and add files from task Ca2-part2 ref#21````  
````$ git push````  
  
* I went to my **Ca3-Part2** and executed the **VagrantFile** to provision and start the two VMs, one that is going to work as a database (for persistence), and other to 
run the Spring application.  
````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca3-part2````  
````$ vagrant up```` 
checking the installation of the two VM  
![VagrantVM](VagrantVM.png)  
* I can then check that both VM´s are running:  
````$ vagrant status````  
and I will see that the two are active ![RunningVM](RunningMachines.png)  
  
* Then I need to make several changes to the Spring Application, for that we follow the commits on https://bitbucket.org/atb/tut-basic-gradle;  
* So first I added support for building the war file to be deployed to tomcat:  
* Adding a plugin to build.gradle:  
```groovy
id 'war'
```  
    
* Creating a ServletInitializer class:  
```java
 import org.springframework.boot.builder.SpringApplicationBuilder;
 
            import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
      
             public class ServletInitializer extends SpringBootServletInitializer {
          
               @Override
                  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
                  return application.sources(ReactAndSpringDataRestApplication.class);
              }
            }
```
            
* Adding support for h2 console, changing the application.properties file:
```
spring.data.rest.base-path=/api  
spring.datasource.url=jdbc:h2:mem:jpadb    
spring.datasource.driverClassName=org.h2.Driver   
spring.datasource.username=sa 
spring.datasource.password=  
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect 
spring.h2.console.enabled=true  
spring.h2.console.path=/h2-console
```                
               
* Adding web-allow to h2:  
```` spring.h2.console.settings.web-allow-others=true````
    
* Changing the path in the app.js:  
```javascript
client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
          			this.setState({employees: response.entity._embedded.employees});
          		})
```  
  
* Adding the path in the application.properties file:  
````server.servlet.context-path=/demo-0.0.1-SNAPSHOT````   
    
* Small changes to the index.html file:  
````<link rel="stylesheet" href="main.css" />````   
     
* Setting for remote h2 database:    
````spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE ````  
this URL will be the one that I have to put on the login dB (jdbc:h2:tcp://192.168.33.11:9092/./jpadb)   
     
* Updating the application properties so that Spring doesn't drop the database on every execution:  
````spring.jpa.hibernate.ddl-auto=update```` 
   
* Finally, to allow spring to run correctly in tomcat8, the application.properties should have this definition:   
````server.servlet.context-path=/demo-0.0.1-SNAPSHOT````  
 ````spring.data.rest.base-path=/api````    
````#spring.datasource.url=jdbc:h2:mem:jpadb````  
````# In the following settings the h2 file is created in /home/vagrant folder````  
````spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE````  
````spring.datasource.driverClassName=org.h2.Driver````  
````spring.datasource.username=sa````  
```` spring.datasource.password=````  
````spring.jpa.database-platform=org.hibernate.dialect.H2Dialect````  
````# So that spring will no drop de database on every execution.````   
````spring.jpa.hibernate.ddl-auto=update````   
````spring.h2.console.enabled=true````  
````spring.h2.console.path=/h2-console````  
````spring.h2.console.settings.web-allow-others=true````      
    
* All changes to the demo file were made individually, as well as the commits, 
however I chose not to indicate them to not be too much detailed.  
    
* Then I have to make some changes to the Vagrantfile, in order to clone my remote repository and for indicate the path were my gradle file
was:  
````$ git clone https://emc285@bitbucket.org/emc285/devops-19-20-a-1191761.git````     
````$ cd devops-19-20-a-1191761/Ca3-Part2/vagrant/demo````  
    - Ensure that both node and node_modules are removed before each buid:  
	```` rm -rf node````  
	```` rm -rf node_modules````  
	- To secure that I have permissions to build I also add a command:  
	````chmod 777 ./gradlew````  
       
* After I made those changes I had commit my upgraded Vagrantfile and commited to my remote repository:
````$ git commit -a -m "changes to the project, readme and vagrantfile"````  
  
* And at least I reboot my VM´s and run the provision steps:  
````$ vagrant reload --provision````  
  
* I don't need to make a Vagrant up, because using vagrant reload --provision I automatically put my VM's running and
run the build, but if I want to power-off my VM's:    
````$ vagrant halt````  
  
* And then I just have to restart my VM's, since there are no changes in my VagrantFile:
````$ vagrant up````  
  
* During this process, I had some troubles with the Vagrantfile configuration, so eventually I need to remove my VM's:  
````$ vagrant halt````    
````$ vagrant destroy````  
  
* And then I have to repeat the process:  
````$ vagrant reload --provision```` 
  
* When the full process was finished and the build done, I used the URL's to access the application:  
  
- http://localhost:8080/demo-0.0.1-SNAPSHOT/    

- http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/  
 
- And to access to database  
 
- http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console    
 
- http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console   


* And has possibly to see all run has expected:  
    * The aplication run well  
    ![RunningApplication](frodo.png)
    * The database also run as expected:  
     ![h2-console](h2_consoleTest.png)  
    * To connect to the H2 we need to use a specific URL, as described on the Vagrantfile: ````jdbc:h2:tcp://192.168.33.11:9092/./jpadb````  
    And we can saw that,  
     ![DB](console.png) 

 
 
___
### Hyper-V as an Alternative to VirtualBox

* As an alternative to VirtualBox I chose Hyper-V, essencially because:  
   * Hyper-V is a native hypervisor written by Microsoft;
     
   * Vagrant comes with support out of the box for Hyper-V.
     Hyper-V is available by default on almost all Windows 8.1 and later versions, in my case I have Windows 10 Pro.
     Since Hyper-V is a Microsoft product, Windows users have a lot of benefits:
        * Familiar Windows interface;  
        * Seamless integration with existing Windows Server management tools;  
        * Full support for guest clusters;  
        * Full support for SR-IOV network adapters, including Live Migration;
        Other pros are the security and stability due to the compartmentalization that comes with Hyper-V, and finally the **Hyper-V Replica** feature, that enables easy cloning
        of servers across a WAN or secure VPN and have them spare in case of a VM outage. Finally, as other hypervisors like Fusion and Virtualbox, Hyper-V is not limited to the user’s device. 
        You can use it for server virtualization, too.  
* But working with Vagrant using Hyper-V has a provider has some limitations too, so:  
   * Vagrant does not yet know how to create and configure new networks for Hyper-V.
     When deploying a machine with Hyper-V, Vagrant will prompt you asking what virtual switch you want to connect the virtual machine to.  
       
   * Networking configurations in the Vagrantfile are completely ignored with Hyper-V, so  Vagrant cannot enforce a static IP or automatically configure NAT.   
     Hyper-V must be enabled prior to using the provider to create VM's (and we cannot use other hypervisor like Virtualbox at the same time, because hyper-v is a Type-1 hypervisor).  
       
   * It's possible to enable Hyper-V using PowerShell, CMD and DISM, or through the Settings. My option was the last one:  
     - I First went to **Control Panel** and selected **Programs**.  
       
     - Selected **Programs and Features** on the right under related settings.   
       
     - Selected **Turn Windows Features on or off**.  
       
     - Selected **Hyper-V** and clicked **OK**.    
        ![Enabled_HyperV](Enable_HyperV.png)  
      
     - Restart the System.  
      
     - Open the Hyper-V manager as administrator.  
      
     - Then we can specify our networks, on the **virtual switch manager** option.  
      
* Prior to using Vagrant to create my VM's, I tried to create one manually, and that was sucessfull, as you can see:  
![VM_manually](VM_instalada.png)  
* So after that I was ready for the implementation of the alternative, using Vagrant to create the VM with a different provider.  

 ____
### Implementation of the Alternative
* Before I started using Vagrant to create the two machines (web and db), like what happened with Ca3-Part2, I had to choose the Vagrant Box that I would use in the Vagrant configuration file.
  There are several alternatives, I opted for one that seemed similar to the one previously used:**"generic/ubuntu1804"**;  
    
* To download the vagrant box, I had to run the command:  
````$ vagrant box add generic/ubuntu1804 --provider hyperv````  
  
* I created a new directory inside Ca3-Part3, **Ca3-alternative**, where I copied the previous VagrantFile of Ca3-Part2:  
````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca3-Part2\Ca3-alternative````  
  
* Almost all the work consisted on changing some lines on the Vagrantfile to adapt to the provider: HyperV.  
  
    * First I had to change all the configurations to the Vagrant Box, for the one that I chose, because "envimation/ubuntu-xenial" doesn't support Hyper-V :  
    ````config.vm.box = "generic/ubuntu1804"```` - the global configuration   
    ````db.vm.box = "generic/ubuntu1804"```` - configurantion of the db VM  
    ````web.vm.box = "generic/ubuntu1804"```` - configuration of the web VM  
      
    * Then I had to change the provider:  
    ````db.vm.provider "hyperv" do |v|```` - for the db VM  
    ````web.vm.provider "hyperv" do |v|```` - for the web VM  
      
    * I made the option of changing the memory of my VM´s, so i had to change that definition on the VagrantFile:  
    ````v.memory = 1024```` - for both of my VM's  
      
    * Then I had to make some changes when I clone my remote repository  
        * clone my repository:  
        ````git clone https://emc285@bitbucket.org/emc285/devops-19-20-a-1191761.git````  
           
        * open where my Gradle executable is:    
        ````cd devops-19-20-a-1191761/Ca3-Part2/vagrant/demo````  
          
        * ensure that previous node and node_modules are removed before the build:  
         ```` rm -rf node```` 
         ```` rm -rf node_modules````  
           
        * change the gradlew permissions:      
        ````chmod 777 gradlew````  
          
* Finally I provisioned the VM's(web and db):  
````$ vagrant up --provider hyperv````  
  
In the end we can see that both VMs are installed and running  
![RunninVM_PowerShell](PowerShellVM.png)  
![RunningVM_HyperV](HyperVMachinesRunning.png)  

* I also needed to change the application.properties file, because the definition of the spring.datasource URL should now
point to the db's IP. For that:  
```` vagrant ssh web````  
  
     * I changed the file application.properties manually:
      ```` sudo nano application.properties````  
         
     * Inside the file:
     ````spring.datasource.url=jdbc:h2:tcp://172.18.108.90:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE````   
     I think I could do that, on my Vagrant configurationFile, but I was not able to find how.  
       
     * Then I ran the build, that generated the apllication's war file:  
     ```` ./gradlew clean buid````  
       
     * To deploy the war file to tomcat8 i had to run the following command:
      ````sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps/basic-0.0.1-SNAPSHOT.war````  
        
     * After that I restarted the Tomcat8 service:  
     ```` sudo service tomcat8 stop````   
     ```` sudo service tomcat8 start````   
       
* Finally I used the URL's to access the web application:    
  
- http://172.18.108.92:8080/basic-0.0.1-SNAPSHOT/  
![web_application](HyperVweb.png)
   
- And to access the database:  
   
- http://172.18.108.92:8080/basic-0.0.1-SNAPSHOT/h2-console  
![db_loggin](HyperVdbFinal.png)   
![db_database](HyperVConsoleDB.png)

##### Notes:
About the alternative, using Hyper-V, as was mentioned before, one of its limitations was that you are unable to set the static IP or forward ports automatically in vagrantfile, 
but I made the option to mantain that configuration because Hyper-v will ignore them.  
       
#### End of Ca3-Part