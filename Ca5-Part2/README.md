# Ca5-Part2  
___
## Create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2)  
* In this task, we will work from the Ca2_Parte2 directory. We'll develop an application, in the demo directory using Spring Data REST and its 
powerful backend functionality. This will be combined with React’s sophisticated features to build an easy-to-understand UI.  
  
* The Ca2_Parte2 was an adaptation of the basic directory of CA1, but here we change from Maven and start a new Gradle spring project adding the necessary dependencies, like
 Rest Repositories; Thymeleaf; JPA; H2.  
  
* The goal of Part 2 of this assignment is to create a pipeline, in Jenkins, to build the tutorial spring boot application;  
For that we need to define a few concepts:  
    * Jenkins is a java open source continuous integration/continuous delivery and deployment (CI/CD) automation tool used in DevOps. 
    It implements CI/CD workflows called pipelines [1];    
    * A pipeline is a set of instructions, given in the form of code, and consists of all instructions needed for the entire build process. 
    With a pipeline, we can build, test, and deliver the application [2];  
     
    ![Pipeline](pipelineINTRO.png)   [3]  

#### Main Goals
##### Stages of the pipeline
* **Checkout**: To checkout the code from its repository;  
* **Assemble**: Compiles and Produces the files that allow the application to run;  
* **Test**: Executes the Unit Tests and publish its results to Jenkins;  
* **Javadoc**: Generates the javadoc for the project and publish it to Jenkins;  
* **Archive**: Archives the generated files (generated during Assemble, i.e., the war file) in Jenkins;  
* **Publish Image**: Generate a docker image with Tomcat and the generated war file and publish it to Docker Hub; 
##### Alternative   
* Implementation of an alternative to Jenkins as a CI/CD tool.


___
## Development

#### Prior tasks  
* Create a Directory Ca5-Part2,a readme file and respective Issue (ref #30):   
````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761````  
````$ mkdir Ca5-Part2````  
````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca5-Part2````  
````$ git status````  
````$ git add Ca5-Part2````  
````$ git commit -m "create a new directory and add a readme.file ref#30"````    
````$ git push````  

* Create a class test for Ca2_Part2 and add some unit tests;   

___
#### Create a Job in Jenkins
* First I create a new Job (a set of instructions that represent how to obtain the artifacts that are intended to be created when building a project/process);
* It won't be a button for "Create a job." Because a job is an item and there are various types of items that can be created.  
* In order to create a new item, click New Item. Give the new item a name:   
    * Defines the Job name: **devops1191761ca5part2**;  
    
    * Defines the type as a Pipeline;  
        * The Pipeline plugin is a step change improvement in the underlying job itself. Unlike freestyle jobs, a Pipeline is resilient to Jenkins master restarts 
        and also has built-in features that supersede many older plugins previously used to build multi-step, complex delivery pipelines [4].  
    
    * Then it's possible to see that I already have two jobs in my Jenkins;  
      
    ![Jobs](JOBS.png)  
___    
#### Configuration of the Pipeline in Jenkins  
* Inside the job **devops1191761ca5part2**, I choose the item configuration;  

* I have to create the Pipeline definition;   
Complex Pipelines are hard to write and maintain within the text area of the Pipeline configuration page. To make this easier, 
Pipelines can also be written in a text editor and checked into source control as a Jenkinsfile which Jenkins can load via the Pipeline Script from SCM option.   
````Pipeline script from SCM````  
  
* Define the SCM as Git;  

* Define credentials because we will be usin DockerHub to push the image created to;  
  

![Docker_Hub_Credentials](credentials.png)  
  
  
* Define the repository URL:  
````https://bitbucket.org/emc285/devops-19-20-a-1191761````  
  
* Define the Script Path:  
````Ca2_Parte2/demo/Jenkinsfile````   
  

![PipelineConfiguration](Pipeline_Browser.png)

___
#### Develop Pipeline as code (Jenkinsfile)  
* First I need to create a new Jenkinsfile in the directory where I have the spring boot application:  
````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca2_Parte2\demo````  
````$ echo Jenkinsfile >> Jenkinsfile````  

* All the script and changes to the Pipeline will be made in this file.  
 
___ 
#### Configure Jenkinsfile
  
                
 * Now that I have created the JenkinsFile, I can start to work in the new pipeline; 
   
    * **pipeline** is the mandatory outer block to invoke the Jenkins pipeline plugin;  
    * **agent** defines where you want to run the pipeline (is a reference to a Node);  
    * **any** is used to identify that any available agent can be used to run the pipeline or stage.  
    
````  
pipeline {
    agent any
````
* Create a stage **Checkout** to checkout the code from the repository;  
````
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/emc285/devops-19-20-a-1191761/src/master/Ca2-Parte2/demo'
            }
        }
````  
* Create a stage **Assemble** to compile and produce the deployment files for the application without running the tests;  
* I use the command dir to work in the directory that I intend to;
* I need to use a **bat** command instead of **sh** because I'm running in a Windows host;  
  
  
![Assemble](assemble.png)  
  
  
````
     stage('Assemble') {
            steps {
               dir("Ca2_Parte2/demo"){
                   echo 'Assembling...'
                   bat 'gradlew assemble'
               }
             }
        }
````  
* Create a **test** stage that will execute the project's tests;  
````
    stage('Test') {
            steps {
               dir("Ca2_Parte2/demo"){
                   echo 'Testing...'
                   bat 'gradlew test'
               }
             }
        }
````  
  
  
* Create a stage **Javadoc** to generate Javadoc and publish it to Jenkins;  
Archive/publish html reports;   
````
        stage('Javadoc') {
             steps {
               dir("Ca2_Parte2/demo"){
                    echo 'Generating Javadoc documentation...'
                    bat './gradlew javadoc'
                    publishHTML (target : [allowMissing: false,
                    alwaysLinkToLastBuild: true,
                    keepAll: true,
                    reportDir: './',
                    reportFiles: 'myreport.html',
                    reportName: 'My Reports',
                    reportTitles: 'The Report'])
                 }
              }
         }
````  
* Here is possible to see the JavaDoc Documentation:  
  
  
![Javadoc_Documentation](javadoc.png)  
  
  
  
* Create a stage to **archive** (in Jenkins) the deployment files, generated during the build;  
This will archive the artifacts, that were generated during the build;  
````
     stage('Archiving') {
            steps {
				dir("Ca2_Parte2/demo"){
                echo 'Archiving...'
                archiveArtifacts 'build/libs/*'
				}
			 }
		}
````  

* The jar and war files generated during the assemble task:   
  
  
![Archive_Files](files.png)  
  


* Generate a **Docker image** with Tomcat and the war file:  
* The --no-cache option is used to ensure that, even if there's no change in its dokerfile, the image will still be built from scratch
````
    stage ('Docker Image') {
            steps {
                dir("Ca2_Parte2/demo"){
                script {
                def test = docker.build("beta85/1191761ca5devops:${env.BUILD_ID}", "--no-cache .")
				docker.withRegistry('https://registry.hub.docker.com ', 'DockerHubCredentials') {
                test.push()
                  }
                }
            }
        }
     }
````  
*  docker.build("beta85/ca5devops:${env.BUILD_ID}", "--no-cache ."):
    * here I need to indicate the name of my DockerHubID: *"beta85"*;  
    * here I define the tag to be added to the newly created image (the number of this Jenkins build): *"{env.BUILD_ID}"*;   
* Then I make the push of the created image;  

  
* To generate a docker image I need to write a dockerfile. This file will have all the commands needed to create the dockerImage:
````
FROM tomcat
````  
*  use a Tomcat DockerImage   
  
  
````
RUN apt-get update -y
````  
* Updates the packages/software available list  
 

````

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN apt-get install nano -y
````  
* To install software;  

````
RUN mkdir -p /tmp/build

RUN mkdir -p /usr/local/tomcat/webapps/ROOT
````  
* Create directories  

````
WORKDIR /tmp/build/
````  
* Set this as the working directory  
  
````
COPY build/libs/demo-0.0.1-SNAPSHOT.war	/usr/local/tomcat/webapps/demo-0.0.1-SNAPSHOT.war
````  
* Copy the file from my machine to this directory/filename on the image  

````
RUN chmod -R 777 /usr/local/tomcat/webapps/
````  
* Grant permissions;  

````
EXPOSE 8080
````  
* Expose the 8080 port on this image;  


* In the end is possible to see that I have a new Image in my DockerHub created with the Jenkinsfile:  
  
  
![Publish image](DockerHub.png)  
    
  
* Make a post stage to publish the test results;  
* Post: defines one or more additional steps that are run after the pipeline finishes.  
    
````
   post {
      always {
        junit '**/test-results/test/*.xml'
      }
   }

} /* pipeline */
````            
  
* It's possible to see the Test Result in Jenkins:   
  
  
![Jenkins_testResult](TestResult.png)  
  

* In the end is possible to validate that the build was executed with no issues, and the stages all ran correctly:  
  

![Build](Build.png)  
  
  
  
* We can also use **Blue Ocean**, and verify that all steps ran sucessfully:   
  
  
  
![](BlueOceanPipeline.png)
    
    
  
* Before I make the build I need to commit to my remote repository the:  
  
* Jenkinsfile to **Ca2_Parte2**:  
````$ git commit -m "create a new Jenkinsfile ref #31"````       
  
* Dockerfile to **Ca2_Parte2**, and only after that can I make the manual build in Jenkins (browser):  
  ````$ git commit -m "create a new dockerfile ref #31"````  


___
## TeamCity as an Alternative to Jenkins  
* At the heart of CI is the tool and technology of the CI server.  The CI server is an application, usually running in the background as a Window service, 
which initiates, executes, and reports on all the processes of the CI lifecycle.  

* The CI server provides key capabilities such as:  
    * Integrating with and monitoring the version control system  
    * Triggering events, such as on source code commits, on a schedule or in sequence  
    * Running build automation and deployment scripts  
    * Running tests  
    * Performing analysis  
    * Providing notification, status messages, and build logs  
    * Storing artifacts and related output  [5]  

* There are many CI servers available, however, TeamCity and Jenkins are two widely-used ones [6];  
   

* JetBrains TeamCity is a user-friendly continuous integration (CI) server for developers and build engineers free of 
charge, and with a Professional Server License available when needed!  
  
* TeamCity's slogan is "Powerful Continuous Integration out of the box" speaks to what they offer in their solution over and above a free option like Jenkins:
  out of the box usability with excellent build history, source control and build chain tools.  
  
* What can you do with TeamCity?﻿  
    * Run parallel builds simultaneously on different platforms and environments;  
  
    * Optimize the code integration cycle and be sure you never get broken code in the repository;  
  
    * Review on-the-fly test results reporting with intelligent tests re-ordering;  
  
    * Run code coverage and duplicates finder for Java and .NET;  
  
    * Customize statistics on build duration, success rate, code quality and custom metrics;  
    
* To understand TeamCity, we need to realize some basic concepts, first the TeamCity build system comprises a server and Build Agents:  
  
  
![TeamCityConcepts](TeamCityDescription.png)   
  
* This is the Basic CI Workflow in TeamCity﻿:  
  
    * The TeamCity server detects a change in your VCS Root and stores it in the database.  

    * The build trigger sees the change in the database and adds a build to the queue.  

    * The server finds an idle compatible build agent and assigns the queued build to this agent.  

    * The agent executes the Build Steps. While the build steps are being executed, the build agent reports the build progress to the TeamCity server sending all the log messages, test reports, code coverage results, etc. to the server on the fly, so you can monitor the build process in real time.  

    * After finishing the build, the build agent sends Build Artifacts to the server [7].    

  

* When comparing Jenkins and TeamCity, the main differences are:  
  
![TeamCity vs Jenkins](Jenkins-vs-TeamCity.png)  
  
  
  * Jenkins is an open source continuous integration tool, well documented while TeamCity is a proprietary offering from JetBrains,with sligtly less investement in documentation;  
    
  * JetBrains offers a free edition as well, with up to 20 build configurations and 3 build agents. Scoping beyond that requires their commercial "enterprise" edition.    
    
  * TeamCity is easier to configure and more straightforward to use, while Jenkins has a rich plugin ecosystem and integrations.  

  * TeamCity has a slick graphical interface and ease of use features make it more palatable for some new to continuous integration.  

  * TeamCity runs in a Java environment, usually on an Apache Tomcat server, though it can be installed on both Windows and Linux servers. TeamCity offers equal support for .NET and openstack projects, integrating into both Visual Studio and IDEs like Eclipse.
TeamCity has converted many Jenkins faithful with its interface and secure default configuration.  

  * Jenkins is widely used and has more users than TeamCity;  

  * Jenkins product update releases are quite frequent and come with some good feature updates and is well documented whereas TeamCity 
is also having frequent releases but documentation is not as extensive.  

  * Jenkins has a steeper learning curve in its usage, installation  and deployment whereas TeamCity has a flatter learning curve in terms of usage, configuration and installation [8][9].  

___
## Implementation of the Alternative - TeamCity

#### TeamCity Installation  

* First, I download the appropriate version for my host, Windows here:[TeamCity](https://www.jetbrains.com/teamcity/download/#section=windows);  
  

* Run the “TeamCity <version number> .exe” file and follow the instructions in the TeamCity Setup wizard. 
The TeamCity web server and a build agent will be installed on the same machine.  

* During installation, I configure the following:  
    * Indicate the directory where TeamCity will be installed, I chose to run directly from my Desktop;  
    * Whether the TeamCity web server and build agent will run as Windows services;  
    * The server port: "80" is TeamCity's default port, which may already be in use by another application on the system.  So I change the server port to 91;  
    * The agent port: "9090" is the default for incoming server connections. But once again I change the "ownPort" value in the Build Agent properties to 9191;  

___  
#### TeamCity Configuration:  
 
* When entering TeamCity's web interface for the first time, I need to:  
  
    * Review the location of the TeamCity Data Directory, where all configuration information is stored. Click on Proceed.  
    * TeamCity stores build history, users, build results and some runtime data in an SQL database and allows you to select the type of database,
     I keep the default internal database (Internal HSQLDB). Click on Proceed.  
    * On the next screen, accept the License Agreement to proceed with the launch. Click on Proceed.  
    * TeamCity displays the Create administrator account page. Specify administrator credentials and click Create Account.
    * I am now connected to TeamCity. Next step is to configure my user settings, create and run my first build [10];    
    
___    
#### TeamCity Build      

* When accessing the TeamCity web interface, the default view will be to show all projects configured for that server.
 To create a project, I use the Administration link in the upper right corner, and then I click in "Create Project";   
   
    
 ![CreateProject](TeamCityNew.png)   
   
   
* Then I can choose to create a project from a URL or manually;  
  
* Since I have my project in Bitbucket I choose to do that from the URL;  
  
* Choose the Parent Project as *Root*;  
  
* So I define the URL, where I have the project that I want to make a build for, and skip the username and password;     
````https://bitbucket.org/emc285/devops-19-20-a-1191761/src/master/Ca2_Parte2/demo/```` 
  
   
![Create_New_Project](CreateProjectURL.png)  
  
  
* After that I have to choose the Project Name: **Devops1191761Ca5** and the Build Configuration Name: **Alternative**;  
  
* TeamCity has an advantage over other CI/CD tools in its auto-detect feature. With this feature, TeamCity can automatically generate some build steps,that we can choose to start from ;    
  
![Automatic_Build_Steps](AutomaticBuildSteps.png)  
   
  
* When I'm inside my project, I choose the Build **Alternative**, to create the necessary steps and change settings;  
  
![ChooseBuild](BuildTeamcity.png)  
  
  
* On the General Settings I have to had a Path to the artifacts:  
````Ca2_Parte2\demo\build\libs => Ca2_Parte2\demo\build\libs````  - to save the generated War file  
   
````Ca2_Parte2\demo\build\docs\javadoc => Ca2_Parte2\demo\build\docs\javadoc```` - to save and publish the generated Javadoc  
  

![Settings_TeamCity](SettingsTeamCity.png)  
  
  
* After that I have to create the build steps (Assemble, Test and Javadoc);    
  

![Build Steps](BuildStepsTeamCity.png)  
   
   
* The assemble task, that compiles and produces the archive files with the application;  
  
  
![Task_Assemble](AssembleTeamCity.png)   
  
* The test task, that executes the Unit Tests and publishes the results to TeamCity;  
  
  
![TaskTest](TestTeamCity.png)  
  
* And finally the step that generates the Javadoc;  
  
  
![Javadoc](JavadocTeamCity.png)  
  
  
* I have to make one more change on *dependencies*, adding a *artifacts paths* to javadoc and War file, that's going to copy the resulting
artifacts to a separate folder in the worker (the archiving step);  
  
![Dependencies](DependenciesTeamCity.png)  
  
  
* Here we can see the build history, and that all tests passed;  
  
  
![Build_History](Build_SucessTeamCity.png)  
  
  
* There's also the possibility of viewing the Build Log and the different steps that ran during the Project Build;  
  
  
![Build_Log](BuildLog.png)  
  
  
* Here we can see the War file tha was generated;  

  
![WarFile](warFileTeamCity.png)    
  
  
* Here we can see the Javadoc Artifact;  
  
![Javadoc_Artifact](JavadocTeamCityResult.png)  

* And at last it's possible to check the test results:  
  
![Test_Result](TestResultTeamCity.png) 

___
## Last Considerations

* During the creation of the Jenkins Pipeline I had some issues with some stages, namely:  
    * **the archiving stage**:  
        * because first I need to find were the war file was saved;  
        * my application was generating a jar file and not a war file;  
    * **docker image** : I had some issues with the credential definition for DockerHub;  

* Comparing Jenkins and TeamCity in terms of which is better is hard, because its related to what people/enterprise are looking for. If you only need a simple tool 
with less plugins, you will like TeamCity (you will have to pay for it though), or a more complicated and less user friendly interface, but well documented like Jenkins.   


___
##

* Finalized the README.md file and commited to CA5-Part2


___
## References
[1](https://www.guru99.com/jenkins-pipeline-tutorial.html)  
[2](https://searchsoftwarequality.techtarget.com/definition/Jenkins)  
[3](https://medium.com/faun/jenkins-pipeline-tutorial-first-step-guide-to-continuous-delivery-87f74d322ab7)  
[4](https://www.cloudbees.com/blog/top-10-best-practices-jenkins-pipeline-plugin)  
[5](https://www.excella.com/insights/teamcity-vs-jenkins-better-continuous-integration-server)  
[6](https://www.upguard.com/articles/teamcity-vs.-jenkins-for-continuous-integration)  
[7](https://www.jetbrains.com/help/teamcity/continuous-integration-with-teamcity.html)  
[8](https://www.upguard.com/articles/teamcity-vs.-jenkins-for-continuous-integration)  
[9](https://dzone.com/articles/cicd-tools-throwdown-jenkins-vs-teamcity-vs-bamboo)  
[10](https://www.jetbrains.com/teamcity/documentation/)  




### End of task Ca5-Part2


