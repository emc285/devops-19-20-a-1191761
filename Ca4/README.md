# Ca4 - Containers with Docker
___

### Introduction to Containers with Docker  
* Docker is a containerization platform that packages applications and all its dependencies together in the form of a docker
container to ensure that the application works seamlessly in any environment.  
    
* Some benefits of working with containers instead of VMs are:
    * Virtual Machines are slow and take a lot of time to boot;
    * Containers are fast and boot quickly as they use the host operating system and share the relevant libraries;
    * Containers do not waste or block host resources unlike virtual machines;
    * Containers have isolated libraries and binaries specific to the application they are running;
    * Containers are handled by Containerization engine.  
      
* Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers.
The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. 
The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.
  
    
* When we use Docker, we are creating and using images, containers, networks, volumes, plugins, and other objects.  
    * **Images**: an image is a read-only template with instructions for creating a Docker container.  
    * **Containers**: a container is a runnable instance of an image. 
    
  
* When we want a multi-container environment, we can use Docker Compose. Compose provides a way to orchestrate multiple containers to work together.
 One example is what we are doing on this task, with a db were we run H2, and a web container running Tomcat8.  
    
    
* To use Docker-Compose we need to describe the system we intend to create. For that, we have to create a YAML file.
    * On the first line we set the version of docker-compose we are using;
    * then we need to specify which containers we are going to create, in other words, which will be the services that Compose will setup;
    * we can also add on some more information, like networks.  
      
      
* Compose has commands for managing the whole lifecycle of your application:
    * Start, stop, and rebuild services;
    * View the status of running services;
    * Stream the log output of running services;
    * Run a one-off command on a service;
    
___    
### Task 4 Development  
  
* I start this task by first installing the Docker Desktop, allowing me to run Docker on my Windows machine. 
Once again I have to enable Hyper-V, because Docker is a Linux Native tool and needs hyper-v to be running. 
After the installation I check that docker is running (on PowerShell):   
```` docker info````  

* Next I download the example available from: https://bitbucket.org/atb/docker-compose-spring-tut-demo/;  

* I open my current working directory in PowerShell as administrator:  
````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761```` 

* Then I create a new directory to this task and a initial README.md file. Then i commit them to my remote repository:    
````$ mkdir Ca4````  
````$ echo Readme >> README.md````  
````$ git status````    
````$ git add Ca4````   
````$ git commit -m "create a new directory and a readme file ref#23, ref#24"````  

* After that I copy the web and db directories, where the dockerfiles for each service are, into my **Ca4**. I then also copy the docker-compose file (a yml file with the commands needed to build both containers). 
 
* On our **docker-compose.yml** we have:
    * the version: '3';
    * the services, or containers, because each container will correspond to one service application:
        * web: where we gonna have the Tomcat running the web application;
        * db: where the H2 server database will be running;  
        * ports: where we map our machine ports to the docker containers ports;
        * networks definitions: for both services, but are not mandatory;
        * depends_on: to specifies that the web service depends on the db service, so db service needs to be run first;
        * volumes: allow us to map folders from our machine to folders on a container;

* Since on the docker-compose file we are building the images from dockerfiles, I need to make some changes:
  
* **web dockerfile**: use a base image for tomcat 
 
* First I have to make some changes to my web dockerfile to adapt to my previous project:
    * I change the cloned repository;  
    ````RUN git clone https://emc285@bitbucket.org/emc285/devops-19-20-a-1191761.git````   
    
    * Change the work directory for the one that I previously used in C3_Part2:   
    ````WORKDIR /tmp/build/devops-19-20-a-1191761/Ca3-Part2/vagrant/demo````  
    
    * Change the copy command, for the definitions that I previously changed on C3-Part2:    
    ````RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/```` 
    
    * I mantain all the other instructions needed to build a spring application.  
    
* **db dockerfile**: uses a base image for ubuntu. The db dockerfile doesn´t suffer any changes.  

* Then I use PowerShell to build the image of my containers:  
```` docker-compose build````  

* With that I expected to create an image for each of my containers, but I came across a gradlew permission error, and I have to change that on
 my web dockerfile, before I made the gradlew build:  
````RUN chmod 777 gradlew````   

* Since my db container is already up I only do a docker-compose build for my web container:   
```` docker-compose build web ```` 

* Once again I have one error during the build of the web container, again with permissions. 
So to make sure that all the actions are allowed, I add another command before my run workdir: 
(Now I can remove the command that give permissions to gradlew)  
````RUN chmod -R 777 /tmp/build/devops-19-20-a-1191761````  

* Lastly I try to build the web image:  
````docker-compose build web ````  
That went with no problems, so I can make the last step, to run web and db containers.  

* Finally I run the docker compose file again, and web and db containers were up:  
```` docker-compose up```` 

- Here we can see that the web spring application is running  
  
![web](web_running.png)
  
  
- And its also possible to verify that both container processes are up:
  
![Process_Running](ps.png)

  
* When the full process was finished and the build done, I used the URL's to access the application:

* Access to web application:  
- http://localhost:8080/demo-0.0.1-SNAPSHOT/  
and we can see the application running:  
  
![web_browser](webApplication.png)  

 
* And to access to database (H2)  
- http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console  
- we first saw the db loggin console page  
  
![lOGIN_db](BDlogin.png)

- and then the db H2   
  
![bd_H2](bd_running.png)  

* When I access my H2 db, I try to add more employee to my database:   
````insert into employee (id, description, first_name, last_name) values(20, 'friend', 'Joana', 'sousa')````  
  
  
![addEmployee](addEmployee.png)    
  

* After that I check my web application:  
  
![webEmployeeAddded](web_employeeadded.png)  
  

* After I confirm that both docker images, web and db, are working as expectedly I publish them to Docker hub.
    * First I have to sign up on Docker hub;  
      
    * Then I can create a repository on docker hub, to where I send the two images, tagged with the current version. I can also do everthing on the PowerShell, and each image will be located in a different repository.  
    I try both ways, but i eventually opted for the second one:  
        * First I have to login to dockerhub (in PowerShell):  
        ````docker login````  
        (that will prompt for a login and a password)  
        * I need to tag my images:  
        ````docker tag ce1953922cec beta85/ca4_db:v1.0```` - db  
        ````docker tag fd0a34da9068 beta85/ca4_web:v1.0```` - web  
        * And then push them to Docker hub:  
        ````docker push beta85/ca4_db```` - db  
        ````docker push beta85/ca4_web```` - web  
          
          
![DockerHub_Images](dockerHub.png)  
  
  
* At last I went to the db container to get a copy of the database file by using exec to run a shell in the 
container and copying the database file to the volume.  
````docker-compose exec db /bin/bash````  

* I saw what is being executed:  
````ps -ax````  

* And I saw the files were created, namely the db file:  
````ls -al````  
  
![db_CreatedFile](dbFile.png)  
  
  
* On the docker-compose file I created a volume, to map files from the container to my desktop:  
```` - ./data:/usr/src/data````   
 
* And I can now copy db file to the data directory on my desktop, wich gives me extra security, that if I lost my container, I wouldn't lose the database data, 
and that i can reuse that information:  
````cp ./jpadb.mv.db /usr/src/data````

![Copy_db_file](copy.png)  

___
### Kubernetes as an alternative to docker/docker-compose 
  

* Kubernetes, is an open source platform that automates the operations of Linux containers.  
Kubernetes was originally created by Google, who had been running similar systems to manage containers as a part of their 
internal infrastructure for many years prior to the announcement of Kubernetes as an open source project in 2014.  
   
* This platform eliminates most of the manual processes needed to deploy and scale applications in containers.
Since Kubernetes' purpose is to fully automate operational tasks, it allows containers to perform many of the tasks made 
possible by other management systems or application platforms.   
  
  
*  Kubernetes can be categorized as a plataform, while Docker Compose  was defined as a "Container" tool.   
*  "Multi-container descriptor", "Fast development environment setup" and "Easy linking of containers" are the key factors 
why developers consider Docker Compose; whereas "Leading docker container management solution", "Simple and powerful" 
are the primary reasons why Kubernetes is favored.  
  
  
* Before this attempt to use kubernetes I applied Docker Compose in development, I containerized my application by:
    * Extracting necessary configuration information from my code.
    * Offloading my application’s state.
    * Packaging my application for repeated use.
    * Defining services that specify how my container images should run.  
      
      
* Now to run my services on a distributed platform like Kubernetes, I will need to translate my Compose service definitions 
to Kubernetes objects. This will allow me to scale my application with resiliency.  
  
* Kubernetes introduces a lot of vocabulary to describe how a application is organized:  
    * **Pods**: a Kubernetes pod is a group of containers, and is the smallest unit that Kubernetes administers;  
    * **Deployments**: Kubernetes deployments define the scale at which you want to run your application by letting you set 
    the details of how you would like pods replicated on your Kubernetes nodes.  
    * **Services**:A service is an abstraction over the pods, and essentially, the only interface the various application consumers interact with.
    As pods are replaced, their internal names and IPs might change. A service exposes a single machine name or IP address mapped to pods whose underlying names and numbers are unreliable.  
    A service ensures that, to the outside network, everything appears to be unchanged.  
    * **Nodes**: a Kubernetes node manages and runs pods; it's the machine (whether virtualized or physical) that performs the given work.  
    * **Master Server**: this is the main entry point for administrators and users to manage the various nodes.  
    * **Cluster**: a cluster is all of the above components put together as a single unit.  

____
### Implementation of the alternative
  
* There are a few steps to take before implementing kubernetes as an alternative to docker compose. 

   * I create a new directory **Kubernetes**, that only have my docker-compose file;
   * I open a new issue, ref #26 to this task;
  
* Related to the alternative itsel:  

   * So, I started by installing Kompose, wich is a tool to help users who are familiar with docker-compose move to Kubernetes. 
    Kompose takes a Docker Compose file and translates it into Kubernetes resources.  
    [Kompose](https://github.com/kubernetes/kompose/blob/master/docs/installation.md)  
   
   
   * Then I installed kubectl, which is a command line tool for controlling Kubernetes clusters.  
    [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-windows)  
  
  
* Since I have a Windows 10 professional version, I'm using Docker-Desktop, and I don't need minikube or other tool to install
kubernetes, I just have to enable kubernetes in my Docker, restart Docker, and make sure that kubernetes is running.  
 
    
* I changed my docker-compose to adapt to kubernetes and to my repository images:  
    * Instead of building a service I use images previous created, so I have to change the build to image;  
    * I change the service names;  
    * I remove unnecessary information like networks, that won't be needed;  
````
version: '3'
services:
  webkubern:
    image: beta85/ca4_web:v1.0
    ports:
      - "8080:8080"
    depends_on:
      - "dbkubern"
  dbkubern:
    image: beta85/ca4_db:v1.0
    ports:
      - "8082:8082"
      - "9092:9092"
````   
  
  
* Then I used the docker client to deploy this to a Kubernetes cluster running the controller:
````$ docker stack deploy --orchestrator=kubernetes -c docker-compose.yml web````  
  

*  I verified that my pods, services, deployment and replicaset:  
````$ kubectl get all````  

This is important to verify the Pods that are running, and my service IP.
![GET_all](kubePodsAndServices.png)

  
* After that I used the URL's to access the application, but for some reason TomCat is returning an error, and I can't access 
my web or db:  
![Kube](kube.png)  
  
* I try to repeat this task to other images, but the result was the same.    
* During this process I realised that I had defined an IP on my application - **resources.application**, and when I up my containers with
kubernetes, that IP the services have changed;  
  
  
* So I need to access to my web container:  
````$ kubectl exec -it webkubernp-7b56499884-wkxqk -- /bin/bash````  
  
* Inside the container executing the following command we proceed to update the database IP (using sed) and rebuild the application as well as make it available on tomcat again.
  
  ````$ sed -i -e 's/192.168.33.11/10.104.124.184/g' src/main/resources/application.properties ; ./gradlew build ; cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/````  

* And as its possible to see, i have access to my web and application:
* Access to web application:  
- http://localhost:8080/demo-0.0.1-SNAPSHOT/  
and we can see the application running:  
  
![web_browser](webKube.png)  

 
* And to access to database (H2)  
- http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console  
- we first saw the db loggin console page, where I need to change the URL login to my db IP(10.104.124.184)  
  
![lOGIN_db](dbConsoleLoginKube.png)

- and then the db H2   
  
![bd_H2](h2Kube.png)  


* At last I made a commit with the directory of the alternative:  
````$ cd C:\Users\just_\Desktop\kubernetes````  
````$ git commit -m "add alternative to ca4 docker-compose ref#26"````  
````$ git push```` 

  
___  
##### Final considerations  

* Kubernetes are an automation application, so manual IP changes should not be necessary.
This should be a setting that would have to be changed in terms of the instructions that the application use;
* During the different attempts, and since I was unable to resolve the Tomcat error, as a possible solution I used other images different of those I previously created, 
it turned out that this was not the problem, but I kept those images;  
* There are different ways of using kubernetes, some explored by me, and that do not imply the use of a previous docker-compose file, however I was unable to implement 
any of these options until it became functional.  
* Kubernetes was not used as an alternative to docker or docker-compose, instead was a complement tool, particular useful when working with several containers.
* Finally, I want to add that the presentation of this alternative was based on particular reference: [kubernetes]( https://www.docker.com/blog/simplifying-kubernetes-with-docker-compose-and-friends/);  

___
###### End of Ca4











 




