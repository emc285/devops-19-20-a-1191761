package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    //test the constructors - HappyCases
    @Test
    @DisplayName("Test Constructor: getFirstName - HappyCase")
    void getFirstName() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description);

        assertEquals(firstName, employeeAna.getFirstName());
    }

    @Test
    @DisplayName("Test Constructor: getLastName - HappyCase")
    void getLastName() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description);

        assertEquals(lastName, employeeAna.getLastName());
    }

    @Test
    @DisplayName("Test Constructor: getDescription - HappyCase")
    void getDescription() {

        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description);

        assertEquals(description, employeeAna.getDescription());
    }



    @Test
    @DisplayName("Test Constructor: getID - HappyCase")

    void getID() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";
        Long id = new Long(123);

        Employee employeeAna = new Employee(firstName, lastName, description);
        employeeAna.setId(id);

        assertEquals(id, employeeAna.getId());
    }


}