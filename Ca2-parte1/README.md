# Ca2 -PART 1 - Gradle
___

## Introduction to Gradle as a Build Tool
___
In this task it is required to use Gradle as a build tool.  

Just to make a brief introduction to the work that will be developed,
I considered important to make a reference to what Gradle is, as a build tool.

Gradle is an advanced build automation system that combines the best of Ant's flexibility with dependency management and Maven conventions.
Gradle build files are scripts written in the Groovy language, unlike Ant and Maven build formats that use XML files for configuration.
Since they are based on scripts, Gradle files allow us to perform programming tasks in a configuration file.
Gradle also has a system of plugins that add extra functionality to its core.

 
___

### Task 2 - Part 1 Development

* To use Gradle as a buid tool, we need to first Install [Gradle Build Toll](https://gradle.org/install/) and add it to the PATH
Windows Environment Variable

* First I start downloading a copy of the files available at : https://bitbucket.org/luisnogueira/gradle_basic_demo/.
 to a folder that I previously created,  named : **Ca2-parte1** and commit to my repository: https://bitbucket.org/emc285/devops-19-20-a-1191761/src/master/ -**(Goal 1)**  
````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca2-Parte1````  
```` $ git commit -a -m "first commit to Ca2 task ref #8"```` 

* Later i followed the steps indicated in the README "Gradle_Basic_Demo" **(Goal 2)**:
    * I started to build a .jar file with the application:  
    ````$ ./gradlew build````
    * Open a terminal and execute the following command from the project's root directory:  
    ```` $ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 ````
    * Open another terminal and execute the following gradle task from the project's root directory:  
    ```` % ./gradlew runClient ````  
    * When performing this task, a window will appear, where the user_name will be requested.
    After its introduction it's possible to start writing messages.  
    ![runClient](Run_Client_.png)  


  
   * To better simulate a real chat situation I opened a new terminal and repeated the command,
    to represent 2 users who can communicate with each other:  
    ![Users_Communication](Comunicação_2_clients.png)  

  

* I added a new task in build.Gradle **(Goal 3)**:
```groovy
       task runServer(type: JavaExec, dependsOn: classes) {
         
          classpath = sourceSets.main.runtimeClasspath
         
          main = 'basic_demo.ChatServerApp'
         
          args '59001'
       }
```
that starts the server through the command:  
```` $ ./gradlew runServer````  

* In **src** I created a new test directory, and added a new class "AppTest".  
To run this test, the junit 4.12 dependency was added to the Gladle script **(Goal 4)**:  
    ```groovy 
    testCompile 'junit:junit:4.12'
  ``` 
    * a new commit was made to the remote repository with the changes made:  
    ```` $ git commit add junit dependency and create a class test ref #10 ````
   
* A new task of type **Copy** was added to be used to make a backup of the sources of the
   application **(Goal 5)**:  
```groovy
   task backupCopy(type: Copy) {
       from 'src'
       into 'backup'
   }
```
   * and we run in the command line the task **Copy**  
    ```` $ ./gradlew backupCopy````  
   * once again a commit was made to add the change to the remote repository:  
    `````$ git commit -a -m "add a copy task ref #11"`````  
    
* A new task of type **Zip** to be used to make an archive of the 
sources of the application **(Goal 6)**: 
```groovy
    task backupZip(type: Zip) {
        archiveFileName = "project.zip"
        destinationDirectory = file("backup")
        
         from "src"
        }
```
   * and we run it on the command line, task **Zip**  
    ```` $ ./gradlew backupZip````  
   * once again a commit was made to add the change to the remote repository:  
    `````$ git commit -a -m "add a task to zip ref #12"`````  


In the end I have a backup folder where I have a copy of the **src**, main and tests, and if I make a ls of that folder
its possible to see the zip.file.
![CopyAndZip](CopyAndZip.png)
___
___
#### End of Ca2-part1
