# Relatório DevOps -  Ca1

## Demonstração de um Cenário de Git Workflow 
___

___
### Descrição da análise, design e implementação dos requisitos
___

Com esta primeira tarefa, Ca1 afectada, pretendia-se simular um cenário de *Git Workflow*, num ambiente de trabalho em que seria possível a criação de diferentes *branches*, partilhando o mesmo repositório remoto por vários *developers*.

O repositório a ser utilizado foi clonado usando as ferramentas do IDE IntelliJ, e foi guardado numa pasta conforme a indicação com o nome de: <devops-19-20-a-1191761>. Foi à *posteriori* criada uma pasta Ca0 com todo o conteúdo existente na pasta  <devops-19-20-a-1191761>, para responder aos requisitos da tarefa 0.  
Aquando da realização do Clone, foi também criado um arquivo .gitignore, que lista aquilo que o git deve ignorar.  
Para responder às premissas alocadas à tarefa Ca1, todo o conteúdo de Ca0 foi copiado para esta, e será desenvolvido o projecto nesta mesma pasta. As alterações serão efectuadas num único ficheiro: **"basic"**.  
Foram analisadas quais as necessidade do cliente e as *features* a serem desenvolvidas para responder às mesmas.
De um modo geral o trabalho foi realizado, seguindo os seguintes passos:  

1. Análise dos requisitos do cliente;
1. Definição de novas *features* a serem desenvolvidas;
1. Criação de Issues para assegurar a concretização das tarefas (realização de diversos commits a referenciar as respectivas issues);
1. Formação de *branches* para responder a cada uma das *features*;
1. Merge desses *branches* com o *master*, quando já foi desenvolvida uma resposta efectiva;
1. Criação de tags para representar as diferentes versões quando estabilizadas;


* Instalação do Git (este passo não foi necessário, uma vez que já tinha sido préviamente instalado)  
[Git](https://git-scm.com/downloads);


* Após instalação podemos confirmar que tudo correru como esperado, utilizando o comando que permite determinar a versão que temos instalada:   
````$ git --version````

* Fazer um set de configurações
    * nome  
    ````$ git config --global user.name "Elisabete Cavaleiro"````
    * email  
    ````$ git config --global user.email "1191761.isep.ipp.pt"````

* Confirmar as configurações  
````$ git config --list````

* Mudar de directório na linha de comandos para o directório onde se encontra a pasta Ca1  
````$ cd 'C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca1'````

* Inicialmente todos os *commits* foram feitos no *Master Branch*, confirmando o mesmo através do comando  
````$ git branch````

* Confirmar quais os ficheiros que estão a aguardar ser adicionados à *Staging Area*, e os ficheiros *Untracked*  
````$ git status````
                                
 * Adicionar esses ficheiros à *Staging Area* e fazer *Commit* dos mesmos. Os *commits* podem ter uma mensagem e referenciar a *Issue*  
````$ git commit -a -m "create directories"````  
Quando fazemos *commit* podemos também optar por não colocar o comentário do mesmo, imediatamente na linha de comando, fazendo apenas  
```` git commit````  
O que irá abrir uma janela, onde deverá ser adicionada a mensagem, contudo achei mais vantajoso usar a primeira alternativa apresentada.
 
* A primeira alteração que foi feita, foi a adição de um ficheiro README.md, seguindo os seguintes passos:
    * criação do mesmo na linha de comandos  
    ````echo 'readme.file' >> README.md````
    * em seguida foi adicionado à *Staging Area* e feito o *commit* e *push*  
    ````git commit -a -m 'add Readme file and other untracked Files [issue 5]'````  
     ````git push ````
 
    * posteriormente foi atribuído um tag de versão inicial a esse mesmo *commit*, usando o comentário do *commit*
        * confirmação do comentário do *commit*  
        ````git log````
        * criação da tag  
        ````git tag v1.2.0 3ba4fbe````
        * fazer o replicação da tag para o repositório remoto  
        ````git push --tags````
 
 * Criaçao de uma nova feature, num novo *branch*, chamado **email-field**  
      ````git branch email-field````  
     * passar a trabalhar nesse *branch*  
     ````git checkout email-field````
     * confirmar que estamos no *branch* correcto  
       ````git branch````  
     * fazer as alterações pretendidas (no IDE IntelliJ) para implementar esta *feature* (criação do campo email, alterações em todas as pastas necessárias, criação de testes..).   
     * podemos confirmar as alterações realizadas no IDE. O que foi modificado irá surgir na Linha de Comandos    
      ````git diff````
     * após a alteração estar estável, assegurando que a *feature* está completa fazemos o merge com o master  
        ````git commit -a -m "add email feature and tests for employee [issue 6]"````       
        ````git push -u origin email-field````

     * de forma a representar o *merge*, visualmente no *Bitbucket*, foi feita também uma pequena alteração no IDE aos testes, e à *DatabaseLoader*, no *MasterBranch*. Foi feito o *commit* dos mesmos
      * Mudar para o *master branch*    
      ````git checkout master````
      * Actualização de eventuais alterações  
      ````git pull origin master````
      * Confirmação da existência de ficheiros a serem adicionados à *staging area*  
       ````git status````
      * Fazer o *commit* de alterações feitas no *master branch*  
       ````git commit -a -m "add some tests to work in branches [issue 6]"````
      * Confirmação dos *branches* que fizeram *merge* com o *master branch*  
        ````git branch --merged````
      * Realizar o *merge* do email-field com o *master*                                                                        
        ````git merge email-field ````
      * Realizar o *push* de todas as alterações                                                                                                                                  
        ````git push origin master````
     * finalmente foi criado um tag para esta nova versão  
        ```` git tag v1.3.0 d66091c````  
        ````git push --tags```` 
      
       
      
 * Uso de um novo *branch* para fazer a correção de eventuais *bugs* no campo de email, com o nome: **fix-invalid-email**.
 Após todas as alterações, e quando tivermos uma versão estável, é feito o *merge* com o *Master*, e será criada um tag, que indique apenas alterações à versão anterior.  
      ````git branch fix-invalid-email````  
      * passar a trabalhar nesse *branch*  
        ````git checkout fix-invalid-email````
      * confirmar que estamos no *branch* correcto  
        ````git branch````
      * fazer as alterações pretendidas  no IDE IntelliJ (usar regex para determinar a criação correcta do email, e lançamento de excepções e respectivos testes). 
      * após a alteração estar estável, assegurando que a *feature* está completa fazemos o merge com o master.  
        ```` git commit -a -m add more test to fix email-bug [issue 7]" ````  
        ```` git push -u origin fix-invalid-email ````
      * de forma a representar o merge, visualmente no Bitbucket, foi feita também uma pequena alteração no IDE aos testes, e à *DatabaseLoader*, no *MasterBranch*                                                                                                                                                                                                                                                                                     
       Foi feito o *commit* dos mesmos
      * Mudar para o *master branch*  
        ````git checkout master````
      * Actualização de eventuais alterações  
        ````git pull origin master````
      * Confirmação da existência de ficheiros a serem adicionados à *staging area*  
       ````git status````
      * Fazer o *commit* de alterações feitas no *master branch*  
        ````git commit -a -m "add more employees"````
      * Confirmação dos *branches* que fizeram *merge* com o *master branch*  
        ```` git branch --merged ````
      * Realizar o *merge* do fix-invalid-email com o *master*  
        ````git merge fix-invalid-email````
      * Realizar o *push* de todas as alterações                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        ````git push origin master````
      * finalmente foi criado um tag para esta nova versão  
      ````git tag v1.3.1 507994e ````  
      ````git push --tags````  
  
                                                    
                                                                
  * Após os *branches* terem sido utilizados, e já não serem mais utilizados podem ser eliminados, para isso:
    * Confirmação dos *branches* que já foram *merged*  
        ````git branch --merged````    
    * Eliminação dos diferentes *branches*
        * Eliminação do *branch* local  
            ````git branch -d email-field````
        * Eliminação do *branch* remoto                                         
            ````git push origin --delete email-field````
        * Eliminação do *branch* local                                                                                   
            ````git branch -d fix-invalid-email````
        * Eliminação do *branch* remoto                                                                                                                      
            ````git push origin --delete fix-invalid-email````                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
___

No final, esta é a imagem geral no Bitbucket, ainda sem a adição da tag Ca1, pois apenas será introduzida após a finalização do ficheiro **README.md**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
![Bitbucket-Commits e Branches](DevOpsCa1.jpg)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
 ___
#### Notas:
 A realização das tarefas email-field e fix-invalid-email, apesar de terem sido feitas utilizando *branches*, uma vez que não estavam a decorrer alterações em mais nenhum branch, após fazer o *merge* com o master não apareciam representadas no *Bitbucket*.
 Assim, foram feitas ligeiras alterações após concluídas as referidas tarefas para ser visualmente mais claro. Os tags só foram atribuídos após estas mesmas alterações.  
 As *Issues* foram por norma apenas incluidas nos *commits* como referência, contudo nos últimos *commits* fez -se o fix das mesmas.  
  Ex: ````git commit -a -m "add one more test - hashcode and fix issue email-field fix #7"````
___

### Fossil como alternativa ao GIT
___
Fossil e Git sobrepõem-se de várias maneiras. Ambos são sistemas de controle de versão distribuídos que armazenam uma árvore de objetos de check-in num clone de repositório local. Em ambos os sistemas, o clone local começa como uma cópia completa do repositório. Novo conteúdo é adicionado ao clone local e, posteriormente, opcionalmente enviado ao repositório remoto, e as alterações no repositório remoto podem ser baixadas para o clone local à vontade. Ambos os sistemas oferecem comparação de versões, aplicação de patches, *branching*, *merging*, *branchs* privadas, etc.

É apresentado como um único executável que deve ser adicionado à *PATH* e que incorpora a funcionalidade de controlo de versões bem como um "issue-tracker", "wiki", Forum, "Technotes" e interface web.
Foi desenvolvido por Richard Hipp (o autor da SQLite) e criado para suportar o desenvolvimento desta. Os repositórios não são mais que uma base de dados SQLite, com todas as vantagens a isso inerentes.

O Fossil demarca-se quando se pretente um sistema distribuído de controlo de versões com um interface de linha de comandos simples e intuitivo, em equipas com uma dimensão relativamente reduzida.

O Git, por outro lado, é um sistema com uma adopção muito maior, o que implica existir um manancial de aplicações "third-party" (Github, Gitlab, Bitbucket, ....) com as quais pode interagir. Este permite também um completo controlo do histórico de alterações, podendo mesmo eliminar quaisquer erros que se possam ter introduzido (no caso do Fossil todas as alterações são imutáveis).

Resumindo, o GIT é um produto maduro que é utilizado por milhões de programadores e projectos de software, e que com a miríade de aplicações "third-party" que existem, permite uma colaboração praticamente infinita.
  
O Fossil é uma proposta mais simples e de nicho, que no entanto incorpora, num único ficheiro, grande parte das funcionalidades necessárias ao processo de desenvolvimento de software, com garantias de que assenta numa tecnologia com provas dadas relativamente a robustez e fiabilidade.



### Implementação do Fossil
___
* Para trabalhar com Fossil, é necessário primeiro fazer a sua instalação, para isso:
    * Abrir a página  e fazer o download para Windows de : [Fossil SCM](https://www.fossil-scm.org/home/uv/download.html)

* Guardar o executável numa pasta, e colocar esta na *PATH* (variáveis de ambiente do Windows).  
   ````C:\Users\just_\Desktop\fossil```` 

* Daí em diante é possível executar o Fossil a partir da linha de comandos, como a *PowerShell*, e podemos confirmar a versão instalada:  
   ````fossil version````

* É possível iniciar um novo projecto ou fazer um clone de um repositório remoto, neste caso optei por criar um novo repositório local:  
    ````fossil init devops.fossil````

* Para fazer com que a pasta local possa estar ao abrigo do controle de versões Fossil:  
    ````fossil open ..\fossil_repos\devops.fossil````
    
* Adicionar todos os ficheiros ao controle de versões:  
    ````fossil add .````

* Para sabermos o estado do repositório local:  
    ````fossil status````

* Para fazer commit é possível através de:  
    ````fossil commit````
    que abre uma janela onde se escreve à *posteriori* a mensagem do *commit*  
    ````fossil commit -m "first commit"````
    a mensagem é imediatamente criada no *commit*  

* para a criação de *branches* para responder às *features*:  
   ````fossil branch new email-field trunk````

* para começar a fazer as alterações nesse *branch*:  
    ````fossil checkout email-field````  
  
Posteriormente foram feitas as alterações no IDE para implementar as *features*

* confirmar se existem alterações:  
    ````fossil status````
   
* após a *feature* ser cumprida faz-se o *commit*:  
      ````fossil commit -m "add email-field" ```` 

* fazer um update para o ramo principal (trunk)- correspondente ao pull do Git:  
    ````fossil update````

* fazer o merge do email-field com o trunk:   
    ````fossil merge email-field````

* finalmente fazer o push:  
    ````fossil push````
    
E repetir para o fix-invalid-email:  

* para a criação de *branches* para responder às *features*:  
   ````fossil branch new fix-invalid-email trunk````

* para começar a fazer as alterações nesse *branch*:  
    ````fossil checkout fix-invalid-email````

* confirmar se existem alterações:  
    ````fossil status````  
  
Posteriormente foram feitas as alterações no IDE para implementar as *features*
   
* após a *feature* ser cumprida faz-se o *commit*:  
      ````fossil commit -m "fix-invalid-email" ```` 

* fazer um update para o ramo principal (trunk)- correspondente ao pull do Git:  
    ````fossil update````

* fazer o merge do email-field com o trunk: 
    ````fossil merge fix-invalid-email````

* finalmente fazer o push:  
    ````fossil push````

  
* adicionar tags aos diferentes pontos de desenvolvimento estáveis (com a indicação do código dos "commits"):  
Pode ser feito no final do projecto, mas idealmente deveram ser adiconadas à medida que se considere que se cumpriu o objectivo da estabilidade das versões.  
  ````fossil tag add v1.2.0 d3351ebf49````   
  ````fossil tag add v1.3.0 e10f62f748````   
 ```` fossil tag add v1.3.1 075de9ba03````  
  ````fossil push```` 
 
       
  ___
No final, esta é a imagem geral na UI para o Repositório local:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
![Fossil - Repositório Local](
local_final.jpg)   
No final, esta é a imagem geral na UI para o Repositório remoto:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
![Fossil - Repositório Remoto](
remote_final.jpg)  
___

#### Notas:
Para simular o envio para um repositório remoto, foi necessário fazer a criação do mesmo.
Os passos acima referidos foram todos executados, de modo a implementar a alternativa escolhida, neste caso Fossil.
___

   

###### Elisabete Cavaleiro 1191761
