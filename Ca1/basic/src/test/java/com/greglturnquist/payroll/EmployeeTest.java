package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    //test the constructors - HappyCases
    @Test
    @DisplayName("Test Constructor: getFirstName - HappyCase")
    void getFirstName() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(firstName, employeeAna.getFirstName());
    }

    @Test
    @DisplayName("Test Constructor: getLastName - HappyCase")
    void getLastName() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(lastName, employeeAna.getLastName());
    }

    @Test
    @DisplayName("Test Constructor: getDescription - HappyCase")
    void getDescription() {

        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(description, employeeAna.getDescription());
    }

    @Test
    @DisplayName("Test Constructor: getJobTitle - HappyCase")
    void getJobTitle() {

        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle,email);

        assertEquals(jobTitle, employeeAna.getJobTitle());
    }

    @Test
    @DisplayName("Test Constructor: getEmail - HappyCase")

    void getEmail() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(email, employeeAna.getEmail());
    }


    @Test
    @DisplayName("Test Constructor: getID - HappyCase")

    void getID() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";
        Long id = new Long(123);

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);
        employeeAna.setId(id);

        assertEquals(id, employeeAna.getId());
    }

   //test the constructors - Exceptions

    @Test
    @DisplayName("Test Constructor: firstNameEmpty")

    void checkConstructer_EmptyFirstName() {

        //Arrange
        String firstName = "";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the firstName parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: firstNameNull")

    void checkConstructer_FirstNameIsNull() {

        //Arrange
        String firstName = null;
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the firstName parameter hasn't a valid argument", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: lastNameEmpty")

    void checkConstructer_EmptyLastName() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the lastName parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: lastNameNull")

    void checkConstructer_LastNameIsNull() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = null;
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the lastName parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: descriptionEmpty")

    void checkConstructer_EmptyDescription() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the description parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: descriptionNull")

    void checkConstructer_DescriptionNull() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = null;
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the description parameter hasn't a valid argument", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: jobTitleEmpty")

    void checkConstructer_EmptyJobTitle() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "";
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the jobTitle parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: jobTitleNull")

    void checkConstructer_JobTitleNull() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = null;
        String email = "anaS@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the jobTitle parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: emailEmpty")

    void checkConstructer_EmptyEmail() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: jobTitleNull")

    void checkConstructer_emailNull() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = null;


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: email without arroba")

    void checkConstructer_emailWithout_arroba_() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "aS.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: email without dot")

    void checkConstructer_emailWithout_dot_() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "aS@hotmailcom";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: email without com")

    void checkConstructer_emailWithout_com_() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "aS@hotmail.c";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: email without text before dot")

    void checkConstructer_emailWithout_textBeforeDot_() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "@hotmail.com";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: email without Domain")

    void checkConstructer_emailwithoutDomain() {

        //Arrange

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(fisrstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }

    //Check Equals

    @Test
    @DisplayName("Test Constructor: getFirstName - HappyCase")
    void checkEquals() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);
        Employee employeeExpected = new Employee(firstName, lastName, description, jobTitle, email);

        assertTrue(employeeAna.equals(employeeExpected));
    }

    //Check HashCode

    @Test
    @DisplayName("Test Constructor: getFirstName - HappyCase")
    void checkHashCode() {
        String firstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";
        String email = "anaS@hotmail.com";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);
        Employee employeeExpected = new Employee(firstName, lastName, description, jobTitle, email);

        assertTrue(employeeAna.hashCode() == employeeAna.hashCode());
    }

}