# CI/CD Pipelines with Jenkins - Using the Basic Demo Project
___

  
### Introduction to Jenkins  

* First of all, I would like to point out that CI means Continuous Integration and CD, Continuous Delivery;  
  
* Continuous Integration is the most important part of DevOps or at least the most precognized. This component is responsible for the integration of the various DevOps stages.  
Continuous Integration is a development practice in which the developers are required to commit changes to the source code in a shared repository 
several times a day or more frequently. Every commit made to the repository is then built automatically [1].  
  
* Jenkins is the most famous Continuous Integration tool;   
![Jenkins](Jenkins.jpg) [2]
  
* Jenkins is an open source automation tool written in Java with plugins purpose built for Continuous Integration.
Jenkins is used to build and test software projects continuously, making it easier for developers to integrate changes to the project,
and making it easier for users to obtain a fresh build. 
It also enables the continuous delivery of software by integrating with a large number of testing and deployment technologies.
  
* So its possible to make a before and after Jenkins, where the main differences are [2]:
    * **Before Jenkins**:  
        * The entire source code was built and then tested. Locating and fixing bugs in the event of build and test failure 
        was difficult and time consuming, which in turn slows down the software delivery process;  
        * Developers have to wait for test results;  
        * The whole process is manual;  
    * **After Jenkins**:  
        * Every commit made to the source code is built and tested. So, instead of checking the entire source code, developers 
        only need to focus on a particular commit. This leads to frequent new software releases;  
        * Developers know the test results of every commit made in the source code;  
        * We only need to commit changes to the source code. Jenkins will then automate the remaing processes for us;  
        
* Jenkins is a very powerful tool. But that doesn't mean that it has no flaws... In fact [3]:
    * Its interface is out dated and not user friendly when taking into account current UI/UX trends;   
    * It's not easy to maintain, because Jenkins runs on a server and requires some skills as a server administrator to monitor its activity.
      One of the reasons why many people don't implement Jenkins is the inherent difficulty in installing and configuring it;
    * Continuous integrations regularly break due to some small setting changes. 
    Continuous integration will be paused and therefore require some developer attention;  
    
* **Blue Ocean** is a plugin that was developed to address the Jenkins UI issues. It's a project that rethinks the user experience of Jenkins, 
modelling and presenting the process of software delivery by surfacing information that’s important to development teams with as few clicks as possible,
while still staying true to the extensibility that is core to Jenkins [4]; 
    
* Jenkins Pipeline is a suite of plugins which supports implementing and integrating continuous delivery pipelines into Jenkins.  
A Pipeline is a user-defined model of a CD pipeline. A Pipeline’s code defines our entire build process, which typically includes 
stages for building an application, testing it and then delivering it [5].

 

___  
### Development  
  
  
#### Prior steps:  
 
* Create a new directory:  
````$ mkdir Ca5-Par5````  

* Create a readme file in the directory:  
````$ echo readme >> README.md````  
````$ git add Ca5-Part1````   
````$ git commit -m "first commit, create directory and add readme ref#27"````  
````$ git push````  
    
___   
#### Jenkins Installation:  
* Go to the official site and install the Jenkins War file, [Jenkins_War](https://www.jenkins.io/doc/book/installing/);  
   
* Download the latest stable Jenkins war;  
   
   
* Put the war executable on the directory where I intend to run the jar command;  
````$ java -jar jenkins.war --httpPort=9090````  
I chose to change the port to avoid future conflicts with other projects that use the 8080 port;
   
   
* Take the password that appears in PowerShell, and paste it to the browser;   
   
![Password](Password.png)
    
* Install suggested plugins;  
They help extend Jenkins capabilities and integrated Jenkins with other software.  
  
* Start using Jenkins, defining username, password, name and email;  

 ___ 
#### Create a new Job [6]:
* The new process that will be executed for the project:  
  
![JenkinsStartingPage](welcomeJ.png)  
  
  
* Define the Job name: **devops1191761**;  

* Choose the pipeline type, in this case **declarative**:
    * Declarative Pipeline syntax offers a simple, predefined hierarchy to make the creation of pipelines and the associated 
    Jenkinsfiles accessible to users of all experience levels.  
   
* Then I start configuring my job;  
It's possible to define several components like, the building triggers, some parameterizations, but the only thing that I changed 
was the pipeline;   
  
I will use a Script from SCM, which means that I have a Jenkinsfile (the script that defines the pipeline) in the root of my project, **Ca2-Parte1**, where I
will define all the Job stages.  
  
___  
#### Configure Pipeline on the browser:  
  
* I have to create the Pipeline definition:  
````Pipeline script from SCM````  
  
* Define the SCM as Git;  

* In my case I didn´t add credentials because I have a public repository;  
  
* Define the repository URL:  
````https://bitbucket.org/emc285/devops-19-20-a-1191761````  
  
* Define the Script Path:  
````Ca2-parte1/Jenkinsfile````  
  
![Pipeline](PipelineDefinition.png)    

___
#### Configure Jenkinsfile
  
                
 * Now I create a JenkinsFile on the Root of **Ca2-Part1** and define the necessary stages/steps:  
 * Starting a new pipeline:    
    * pipeline is the mandatory outer block to invoke the Jenkins pipeline plugin;  
    * agent defines where you want to run the pipeline (was a reference to a Node);  
    * any says to use any available agent to run the pipeline or stage.  
    
````  
pipeline {
    agent any
````
* Create a stage to checkout the code from the repository;  
````
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/emc285/devops-19-20-a-1191761/src/master/Ca2-parte1'
            }
        }
````  
* Create a stage to compile and produce the deployment files for the application;  
* I need to use a **bat** command instead of **sh** because I'm running in a Windows environment;  
* I make an option to use a gradlew build task and to doens't allow the task test to run;  
* I make a clean before the build to ensure that all the files are updated;  
* And I add a rerun command to make sure that all tasks restart when I make a new build;   
````
        stage('Assemble') {
            steps {
               dir("Ca2-parte1"){
                   echo 'Assemble...'
                   bat 'gradlew clean build --rerun-tasks -x test'
               }
             }
        }
````  
* Create a test stage that will execute the project's tests;  
````
        stage('Test') {
             steps {
                dir("Ca2-parte1"){
                    echo 'Testing...'
                    bat 'gradlew test'
                 }
              }
         }
````  
  
* Create a stage to archive (in Jenkins) the deployment files, generated during the first stage;  
````
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }

   } /* Stages*/
````  
  
* Make a post stage to publish the test results;  
* Post: defines one or more additional steps that are run after the pipeline finishes.  
    
````
   post {
      always {
        junit '**/test-results/test/*.xml'
      }
   }

} /* pipeline */
````            

* I need to commit the Jenkinsfile for the **Ca2-Part1**, and only after that can I make the manual build in Jenkins (browser):  
````$ git commit -m "create a JenkinsFile ref#28"````       

___
#### Building the Job  
  
* I have to go to Jenkins in the browser and make a build of the job;  
    
* Then, if the build ran successfully, I can check that all the stages become green, and the build that was running turned blue;  
  
![Build_Sucess](Build_Sucess.png)  
    
    
* If I go to the console output, I can choose **workspace**. This allows us to go to the Ca2-parte directory, and open the files
and ensure that everthing ran as expected:  
     
![Jenkins_workSpace](workSpace.png)  
  
* Its also possible to choose the Test Result, and verify, that it ran without failures:  
  
![TestVerification](tests.png)  

* I only have one test, but was possible to see that it passed:  
  
![TestPassed](TestPassed.png)  
  
![TestResult](TestResult.png)  
  
* Finally I can use a friendlier UI like **BlueOcean**, and see the same results:  
  
![Pipeline](BlueOceanGeral.png)  
  
![Pipeline and Builds](BlueOceanBuilds.png)  
  

* It's important to note, that during all this process Jenkins must be running in PowerShell, because Jenkins is a server, 
a service that will be executing, so I can't finish or stop the running process in the PowerShell.  

* In the end, on the initial page of my Jenkins, it's possible to see, that is not completely a sunny day, because I have
some troubles to make everthing run, and several builds failed;   
![Jenkins_first_page](Jenkins.png)  


___
#### Last steps to finish the task  


* For last I make a commit with the Readme file;

* And add a final tag for this task, Ca5-Part1; 
  
___
#### Bibliographic References  
- [1](https://www.thoughtworks.com/continuous-integration)  
- [2](https://www.edureka.co/blog/what-is-jenkins/)
- [3](https://thenewstack.io/many-problems-jenkins-continuous-delivery/)
- [4](https://www.jenkins.io/blog/2016/05/26/introducing-blue-ocean/)
- [5](https://www.jenkins.io/doc/book/pipeline/getting-started/)
- [6](https://opensource.com/article/19/9/intro-building-cicd-pipelines-jenkins)

                    
___        
##### End of Ca5-Part1       
        
    