# Ca3 - Part1 - VirtualBox with Ubuntu
___
### Introduction to Virtual Machine

A virtual machine is a computer file, typically called an image, that behaves like an actual computer. 
In other words, creating a computer within a computer.
There are many reasons to use virtualization:  
1. by virtualizing our server, we can place many virtual servers onto each physical server to improve hardware utilization;  
2. we don't need to purchase additional physical resources, like hard drives or hard disks;  
3. a VM provides an environment that is isolated from the rest of a system, so whatever is running inside a VM won’t interfere with anything else running on the host hardware;  
4. since VMs are isolated, they are a good option for testing new applications or setting up a production environment.   

Software called a hypervisor separates the machine’s resources from the hardware and provisions them appropriately so that they can be used by the VM. 
To perform this task, the hypervisor that was used was VirtualBox. VirtualBox is a typeII hypervisor. This type of hypervisor requires a host OS to be run on.

### Task 3 - Part1: Development
___

* Before the starting of **task3 part1**, a new VM was created, using VirtualBox, and later Linux (Ubuntu) was installed on that VM.
This whole process will not be explained by me,because I consider it to be a pre-requisite to carrying out this particular task.
**(Goal 1)**
* After that, I open a Windows PowerShell
    * went to my repository:  
    ````$ cd C:\Users\Just_\Desktop\devops-19-20-a-1191761````  
    * create a directory to the new task  
    ````$ mkdir Ca3-Part1````  
    * create a README file  
    ````$ echo README >> README.md````  
    * make a new Issue in Bitbucket : *Issue 20*  
    * confirm that I have changes to commit and add them to the *Staging Area*  
    ````$ git status````
    ````$ git add Ca3-Part1````  
    * commit those changes and push them to the remote repository  
    ````$ git commit -m "create a new directory and a README file for task Ca3-Part1 ref#20"````  
    ````$ git push````  
   
* From now on, all the processes I'll explain will be carried out on the VM command line, with the exception of those that will be highlighted as such.  
* Start to install dependencies, that will be needed to implement the different projects, some off them,I already have done during the creation of the VM.  
    * Install GIT  
        ````$ sudo apt install git````  
    * Install JDK    
         ````$ sudo apt install openjdk-8-jdk-headless````  
    * Install Maven   
        ````$ sudo apt install maven````  
    * Install Gradle  
        ````$ sudo apt install gradle````    
    * Clone my individual repository inside the VM  
   ````$ git clone https://emc285@bitbucket.org/emc285/devops-19-20-a-1191761.git```` 
![Git clone my repository](cloneRepo.png)  
**(Goal 2)**  
    * After that, I made a list of the files and directories on my VM to confirm that the clone had been executed correctly:  
    ````$ ls ````  
    * I change to the directory that was cloned:  
    ````$ cd devops-19-20-a-1191761````  
    * Open the **basic** directory, where I have the spring boot tutorial basic project with the changes that I made:  
    ````$ cd Ca1\basic````   
    * when I tried to make the spring-boot run, that wasn't possible, because of some permissions issues, namely with the mvnw file, so I had to make some changes to its permissions:  
    ````$ chmod 777 mvnw````   
    * lastly I try to execute again the basic spring boot tutorial:
    ````$ ./mvnw spring-boot:run````  
    * then I open the application using the host computer's browser, and the URL:
    ```` http://192.168.56.100:8080/````  
    * Everything went has expected, as It's possible to see in the following picture:  
![Spring Application](localHostBasic.png)  
**(Goal 4)**  

    * Next I changed to the directory where I have my Gradle project:  
    ````$ cd devops-10-20-a-1191761/Ca2_parte2````
    * I listed my files and directories to check permissions, and once again I had to change the permission of the gradlew file:    
    ````$ chmod 777 gradlew````
    * I execute the gradle project:  
    ````$ ./gradlew build````  
    * And I run the server in my VM:  
    ```` ./gradlew runServer````   
    ![RunServer](RunServer.png)
    
    * To run the client, task **runClient** we must use a different command line, outside the VM, because our VM doesn't have a graphic environment.
  So for that I chose to use the Windows PowerShell.  
    * First I have to open the directory in the PowerShell:  
        ````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca2-Parte1````
    * Then I have to execute the Gradle **build** task:  
        ````$ ./gradlew build````  
    * After that I run the **runClient** task:  
        ````$ ./gradlew runClient````  
        
    * But in this case I had an error, since in the **runClient** task I had set the "localhost" in its arguments, which in this case would not be pointing to the server's (VM) IP.    
    So in my first attempt to solve this issue, I changed the argument 'localhost' to the IP of my VM (192.168.26.100), 
    and i repeated all the steps in PowerShell:  
    * First I have to open the directory in the PowerShell:  
     ````$ cd C:\Users\just_\Desktop\devops-19-20-a-1191761\Ca2-Parte1````  
    * Then I have to execute the Gradle **build** task:    
     ````$ ./gradlew build````   
    * After that I run the **runClient** task:  
     ````$ ./gradlew runClient````     
    ![RunClient_IP](run_Client_Ca3_part1.png)  
    * This worked and it was possible to run the client, but it raised some doubts whether it would be the most correct way. 
    Since I would be limiting it to just one IP, and whenever we wanted to run it from another machine, 
    we would have to change the runClient task code.
    So, I went for an alternative, which seemed to better meet expectations. 
    I removed the argument from the runClient task, where I explictly set the IP and port, 
    and started to set them on the command line.  
   That's why I change the the code for the **runClient** task was changed:   
```groovy
       task runClient(type:JavaExec, dependsOn: classes){
           group = "DevOps"
           description = "Launches a chat client that connects to a server on localhost:59001 "
       
           classpath = sourceSets.main.runtimeClasspath
       
           main = 'basic_demo.ChatClientApp'
       
           /*args '192.168.56.100', '59001'
       
            */
       }
```
 
   * After that I, once again, repeated the full process, with one change. When I  execute the **runClient** task:  
    ````$ ./gradlew runClient --args '192.168.56.100 59001'````      
    that works, and as expected it's possible to run several clients, opening different Windows PowerShell.  
    ![RunClient](ClientsComunicationArguments.png)  
    **(Goal 3 and 5)**

___       
#### End of Ca3-Part1  
___    