package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getFirstName() {
        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";

        Employee employeeAna = new Employee(fisrstName, lastName, description, jobTitle);

        assertEquals(fisrstName, employeeAna.getFirstName());
    }

    @Test
    void getLastName() {
        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";

        Employee employeeAna = new Employee(fisrstName, lastName, description, jobTitle);

        assertEquals(lastName, employeeAna.getLastName());
    }

    @Test
    void getDescription() {

        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";

        Employee employeeAna = new Employee(fisrstName, lastName, description, jobTitle);

        assertEquals(description, employeeAna.getDescription());
    }

    @Test
    void getJobTitle() {
        String fisrstName = "Ana";
        String lastName = "Silva";
        String description = "Software Developer";
        String jobTitle = "Engineer";

        Employee employeeAna = new Employee(fisrstName, lastName, description, jobTitle);

        assertEquals(jobTitle, employeeAna.getJobTitle());
    }
}